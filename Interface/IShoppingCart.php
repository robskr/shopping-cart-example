<?php

declare(strict_types=1);

namespace Espresso\EcommerceBundle\ShoppingCart;

use Espresso\EcommerceBundle\Entity\Shipping;
use Espresso\EcommerceBundle\Entity\Payment;

interface IShoppingCart
{
    public function add(IShoppingCartItem $item, int $quantity): void;


    public function update(IShoppingCartItem $item, int $quantity): void;


    public function remove(string $key): void;


    public function isEmpty(): bool;


    public function getItem(string $key): IShoppingCartItem;


    /**
     * @return IShoppingCartItem[]
     */
    public function getItems(): iterable;


    public function getItemCount(): int;


    public function getQuantityCount(): int;


    public function getSubtotal(): string;


    public function getTotal(): string;


    public function setShipping(?Shipping $shipping): void;


    public function setPayment(?Payment $payment): void;


    public function getCurrency(): string;


    public function destroy(): void;
}
