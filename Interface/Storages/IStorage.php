<?php

declare(strict_types=1);

namespace Espresso\EcommerceBundle\ShoppingCart\Storages;

use Espresso\EcommerceBundle\ShoppingCart\IShoppingCartItem;

interface IStorage
{
    /**
     * @return IShoppingCartItem[]
     */
    public function getItems(): iterable;


    public function getItem(string $key): ?IShoppingCartItem;


    public function add(IShoppingCartItem $item, int $quantity): void;


    public function update(IShoppingCartItem $item, int $quantity): void;


    public function remove(string $key): void;


    public function destroy(): void;
}
