<?php

declare(strict_types=1);

namespace Espresso\EcommerceBundle\ShoppingCart;

interface IShoppingCartItem
{
    public function getKey(): string;


    public function getName(): string;


    public function getSku(): string;


    public function getUnitPrice(): string;


    public function getQuantity(): int;


    public function getPrice(): string;
}
