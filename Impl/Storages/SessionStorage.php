<?php

declare(strict_types=1);

namespace Espresso\EcommerceBundle\ShoppingCart\Storages;

use Espresso\EcommerceBundle\ShoppingCart\IShoppingCartItem;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\SmartObject;
use function array_key_exists;

class SessionStorage implements IStorage
{
    use SmartObject;

    /** @var SessionSection */
    private $cart;


    public function __construct(Session $session)
    {
        $this->cart = $session->getSection(self::class);
    }


    public function add(IShoppingCartItem $item, int $quantity = 1): void
    {
        if (isset($this->cart[$item->getKey()])) {
            $this->cart[$item->getKey()]->quantity += $quantity;
        } else {
            $this->cart[$item->getKey()] = $item;
        }
    }


    public function remove(string $key): void
    {
        unset($this->cart[$key]);
    }


    public function update(IShoppingCartItem $item, int $quantity): void
    {
        if ($quantity <= 0) {
            $this->remove($item->getKey());
        }

        $this->cart[$item->getKey()] = $quantity;
    }


    public function getItem(string $key): ?IShoppingCartItem
    {
        return array_key_exists($key, $this->getItems()) ? $this->cart[$key] : null;
    }


    /**
     * @return IShoppingCartItem[]
     */
    public function getItems(): iterable
    {
        return $this->cart->getIterator()->getArrayCopy();
    }


    public function destroy(): void
    {
        $this->cart->remove();
    }
}
