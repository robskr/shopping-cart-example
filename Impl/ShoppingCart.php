<?php

declare(strict_types=1);

namespace Espresso\EcommerceBundle\ShoppingCart;

use Espresso\EcommerceBundle\Entity\Shipping;
use Espresso\EcommerceBundle\Entity\Payment;
use Espresso\EcommerceBundle\ShoppingCart\Storages\IStorage;
use Nette\SmartObject;
use function bcadd;
use function bcmul;
use function count;

class ShoppingCart implements IShoppingCart
{
    use SmartObject;

    /** @var IStorage */
    private $storage;

    /** @var string */
    private $subtotal = '0';

    /** @var string */
    private $total = '0';

    /** @var Shipping|NULL */
    private $shipping;

    /** @var Payment|NULL */
    private $payment;


    public function __construct(IStorage $storage)
    {
        $this->storage = $storage;
    }


    public function isEmpty(): bool
    {
        return count($this->getItems()) === 0;
    }


    public function add(IShoppingCartItem $item, int $quantity = 1): void
    {
        $this->storage->add($item, $quantity);
    }


    public function remove(string $key): void
    {
        $this->storage->remove($key);
    }


    public function update(IShoppingCartItem $item, int $quantity): void
    {
        $this->storage->update($item, $quantity);
    }


    public function getItem(string $key): IShoppingCartItem
    {
        return $this->storage->getItem($key);
    }


    /**
     * @return IShoppingCartItem[]
     */
    public function getItems(): iterable
    {
        return $this->storage->getItems();
    }


    public function getItemCount(): int
    {
        return count($this->getItems());
    }


    public function getQuantityCount(): int
    {
        $items = $this->getItems();
        $quantityCount = 0;

        foreach ($items as $item) {
            $quantityCount += $item->getQuantity();
        }

        return $quantityCount;
    }


    public function getSubtotal(): string
    {
        $this->updateTotals();

        return $this->subtotal;
    }


    public function getTotal(): string
    {
        $this->updateTotals();

        return $this->total;
    }


    public function destroy(): void
    {
        $this->storage->destroy();
    }


    public function setShipping(?Shipping $shipping): void
    {
        $this->shipping = $shipping;
        $this->updateTotals();
    }


    public function getShipping(): ?Shipping
    {
        return $this->shipping;
    }


    public function setPayment(?Payment $payment): void
    {
        $this->payment = $payment;
        $this->updateTotals();
    }


    public function getPayment(): ?Payment
    {
        return $this->payment;
    }


    public function getCurrency(): string
    {
        return 'CZK';
    }


    private function updateTotals(): void
    {
        $this->subtotal = '0';

        foreach ($this->getItems() as $item) {
            $this->subtotal = bcadd($this->subtotal, bcmul((string) $item->getQuantity(), $item->getUnitPrice(), 2), 2);
        }

        $this->total = $this->subtotal;

        if ($this->shipping === null && $this->payment === null) {
            return;
        }

        $this->total = bcadd($this->total, $this->shipping->getPrice(), 2);
        $this->total = bcadd($this->total, $this->payment->getPrice(), 2);
    }
}
