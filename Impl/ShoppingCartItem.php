<?php

declare(strict_types=1);

namespace Espresso\EcommerceBundle\ShoppingCart;

use Espresso\EcommerceBundle\Entity\Product;

class ShoppingCartItem implements IShoppingCartItem
{
    /** @var Product */
    private $product;

    /** @var int */
    private $quantity;


    public function __construct(Product $product)
    {
        $this->product = $product;
    }


    public function getKey(): string
    {
        return (string) $this->product->getId();
    }


    public function getName(): string
    {
        return $this->product->getName();
    }


    public function getSku(): string
    {
        return $this->product->getSku();
    }


    public function getQuantity(): int
    {
        return $this->quantity;
    }


    public function getUnitPrice(): string
    {
        return $this->product->getUnitPrice();
    }


    public function getPrice(): string
    {
        return bcmul($this->product->getUnitPrice(), (string) $this->getQuantity(), 2);
    }


    public function getProduct(): Product
    {
        return $this->product;
    }
}
